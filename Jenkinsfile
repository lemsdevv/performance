pipeline {
   agent any

   parameters {
      string(name: 'ENV_URL', defaultValue: 'training.flooded.io', description: 'URL to be tested in JMeter Test Plan')
      string(name: 'VUSERS', defaultValue: '2', description: 'Count of threads (virtual users)')
      string(name: 'RAMP_UP', defaultValue: '1', description: 'Time to take all chosen threads')
      string(name: 'LOOP_COUNT', defaultValue: '-1', description: 'Count of loops to proceed test')
      string(name: 'DURATION', defaultValue: '60', description: 'Duration time for testing')
      string(name: 'SCENARIO', defaultValue: 'Jmeter_Floodio_Test_Plan.jmx', description: 'Name of the *.jmx test file')
      string(name: 'RANDOM_DELAY', defaultValue: '2000', description: 'Max random delay in millies to add to think time')
      string(name: 'CONST_DELAY', defaultValue: '1000', description: 'Constant delay in millies of think time')
      string(name: 'JMETER_VERSION', defaultValue: '5.2.1', description: 'JMeter version you choose for testing')
   }

   
   stages {
      stage('workspace-clean-up') {
         steps {
            cleanWs()
         }
      }
      stage('clone_task_branch') {
         steps {
            git branch: '#2-jmeter-floodio-task', url: 'https://gitlab+deploy-token-152592:9GfyANDE4zef4PACddh-@gitlab.com/lemsdevv/performance.git'
         }
      }
      stage('setup_and_run_test') {
            steps {
               dir("jmeter/") {
                  sh "chmod +x launch.sh"
                  sh "docker build -t jmeter . --build-arg JMETER_VERSION=${JMETER_VERSION}"
                  sh '''docker run \
                     --volume /var/lib/jenkins/workspace/FloodedioJmeter/jmeter-floodio:/mnt/jmeter \
                     --network mydocker_default \
                     jmeter \
                     -n \
                     -Jjmeter.save.saveservice.output_format=csv \
                     -JURL=${ENV_URL} \
                     -JTHREADS=${VUSERS} \
                     -JRAMP_UP=${RAMP_UP} \
                     -JLOOPS=${LOOP_COUNT} \
                     -JDURATION=${DURATION} \
                     -JRANDOM_DELAY=${RANDOM_DELAY} \
                     -JCONST_DELAY=${CONST_DELAY} \
                     -t /mnt/jmeter/${SCENARIO} \
                     -l /mnt/jmeter/tmp/result.jtl \
                     -e \
                     -o /mnt/jmeter/report'''
               }
            }
      }
     stage('generate_report') {
        steps {
           perfReport compareBuildPrevious: true, excludeResponseTime: true, filterRegex: '', modeThroughput: true, sourceDataFiles: '**/result.jtl'
        }
     }
   }
}
